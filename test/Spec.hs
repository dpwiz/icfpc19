import Data.Foldable (for_)
import Text.Printf (printf)
import Text.Show.Pretty (ppShow)

import qualified Text.Megaparsec as P

import qualified Parser
import qualified Printer

main :: IO ()
main =
  for_ taskSets $ \(setName, tasks) -> do
    putStrLn $ "Set: " <> setName
    for_ tasks $ \srcName -> do
      putStrLn $ "Task: " <> srcName
      src <- readFile srcName

      task <- case P.runParser Parser.parseTaskFile srcName src of
        Left peb ->
          fail $ P.errorBundlePretty peb
        Right task ->
          pure task

      let printed = Printer.task task
      if printed == src then
        pure ()
      else
        fail $ unlines
          [ "Printer does not match original:"
          , ppShow $ zip src printed
          ]

taskSets :: [(String, [FilePath])]
taskSets =
  [ mkSet "part-1-initial"   (1,   150)
  , mkSet "part-2-teleports" (151, 220)
  , mkSet "part-3-clones"    (221, 300)
  ]

mkSet :: String -> (Int, Int) -> (String, [FilePath])
mkSet setName (taskStart, taskEnd) = (setName, taskFiles)
  where
    taskFiles =
      map (printf "data/%s/prob-%03d.desc" setName) [taskStart .. taskEnd]
