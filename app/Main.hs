module Main where

import System.Environment (getArgs)
import Text.Show.Pretty (ppShow)

import Parser (readTaskFile)

main :: IO ()
main = getArgs >>= mapM_ process

process :: FilePath -> IO ()
process fp = do
  task <- readTaskFile fp
  putStrLn $ ppShow task
