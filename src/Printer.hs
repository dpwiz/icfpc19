module Printer where

import Prelude hiding (map)
import qualified Data.List as List

import Types

type Doc = String

task :: Task -> Doc
task Task{..} = mconcat
  [ map taskMineMap
  , "#"
  , point taskInitialLoc
  , "#"
  , obstacles taskObstacles
  , "#"
  , boosters taskBoosters
  ]

map :: Map -> Doc
map = List.intercalate "," . List.map point

point :: Point -> Doc
point (Point x y)= mconcat
  [ "("
  , show x
  , ","
  , show y
  , ")"
  ]

obstacles :: [Map] -> Doc
obstacles = List.intercalate ";" . List.map map

boosters :: [(Point, Booster)] -> Doc
boosters = List.intercalate ";" . List.map pb
  where
    pb (p, b) = boosterChar b : point p
