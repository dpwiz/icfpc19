module Types where

-- * Input

data Task = Task
  { taskMineMap    :: Map
  , taskInitialLoc :: Point
  , taskObstacles  :: [Map]
  , taskBoosters   :: [(Point, Booster)]
  } deriving (Show)

type Map = [Point]

data Point = Point !Int !Int
  deriving (Eq, Show)

data Booster
  = BExtension
  | BFastWheels
  | BDrill
  | BMystery
  | BTeleport
  | BClone
  deriving (Eq, Ord, Enum, Bounded, Show)

boosterChar :: Booster -> Char
boosterChar = \case
  BExtension  -> 'B'
  BFastWheels -> 'F'
  BDrill      -> 'L'
  BMystery    -> 'X'
  BTeleport   -> 'R'
  BClone      -> 'C'

-- * Output

-- | A solution is valid, if it does not force the worker-wrapper
-- to go through the walls and obstacles (unless it uses a drill),
-- respects the rules of using boosters, and, upon fnishing,
-- leaves all reachable squares of the map wrapped.
type Solution = [Action]

data Action
  -- Initial
  = AMoveUp              -- ^ W
  | AMoveDown            -- ^ S
  | AMoveLeft            -- ^ A
  | AMoveRight           -- ^ D
  | ADoNothing           -- ^ Z
  | ATurnCW              -- ^ E
  | ATurnCCW             -- ^ Q
  | AAttachManip Int Int -- ^ B(rx,ry)
  | AAttachWheels        -- ^ F
  | AStartDrill          -- ^ L
  -- Teleport
  | AReset               -- ^ R
  | AShift               -- ^ T(x,y)
  -- Clones
  | AClone               -- ^ C
  deriving (Eq, Ord, Show)
