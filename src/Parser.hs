module Parser where
  -- ( taskFile
  -- , Parser
  -- ) where

import Data.Functor (($>))
import Data.Void (Void)
import Text.Show.Pretty (ppShow)

import qualified Text.Megaparsec as P
import qualified Text.Megaparsec.Char as P
import qualified Text.Megaparsec.Char.Lexer as L

import Types (Task(..), Map, Point(..), Booster(..))

readTaskFile :: FilePath -> IO (Either String Task)
readTaskFile srcName = do
  src <- readFile srcName
  pure $
    case P.runParser parseTaskFile srcName src of
      Left peb ->
        Left (P.errorBundlePretty peb)
      Right res ->
        Right res

type Parser = P.Parsec Void String

parseTaskFile :: Parser Task
parseTaskFile =
  parseTask <* P.skipManyTill P.space1 P.eof

parseTask :: Parser Task
parseTask = Task
  <$> parseMap <* P.char '#'
  <*> parseInitialLoc <* P.char '#'
  <*> parseObstacles <* P.char '#'
  <*> parseBoosters

parseMap :: Parser Map
parseMap =
  parsePoint `P.sepBy` P.char ','

parseInitialLoc :: Parser Point
parseInitialLoc = parsePoint

parseObstacles :: Parser [Map]
parseObstacles =
  parseMap `P.sepBy` P.char ';'

parseBoosters :: Parser [(Point, Booster)]
parseBoosters =
  parseBooster `P.sepBy` P.char ';'

parseBooster :: Parser (Point, Booster)
parseBooster = flip (,)
  <$> P.choice
        [ P.char 'B' $> BExtension
        , P.char 'F' $> BFastWheels
        , P.char 'L' $> BDrill
        , P.char 'X' $> BMystery
        , P.char 'R' $> BTeleport
        , P.char 'C' $> BClone
        ]
  <*> parsePoint

-- * Components

parsePoint :: Parser Point
parsePoint = Point
  <$> (P.char '(' *> L.decimal)
  <*> (P.char ',' *> L.decimal)
  <* P.char ')'
